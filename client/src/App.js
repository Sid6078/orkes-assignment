import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [collection, setCollection] = useState([]);
  const [loading, setLoading] = useState(true);
  const [curPage, setCurPage] = useState(1);
  const getData = async (url) => {
    const res = await fetch(url, {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    });
    if (res.ok) {
      const result = await res.json();
      setCollection((prevNodes) => [...prevNodes, ...result.nodes]);
      setLoading(false);
    } else {
      setCollection([]);
    }
  };

  window.onscroll = function () {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      // load more data
      setCurPage((prevPage) => prevPage + 1);
    }
  };

  useEffect(() => {
    const url = `http://localhost:8080/api/${curPage}`;
    getData(url);
  }, [curPage]);

  if (loading) {
    return <div>Loading...</div>;
  }

  const trimEllip = function (str, length) {
    return str.length > length ? str.substring(0, length) + "..." : str;
  };
  return (
    <div className="App">
      <div className="main-wrapper">
        {!loading &&
          collection &&
          collection.map((post) => {
            return (
              <div key={post.node.nid} className="wrapper">
                <img
                  src={post.node.field_photo_image_section}
                  alt="profile"
                  className="profile-photo"
                />
                <div className="date-title-wrapper">
                  <div className="title">{trimEllip(post.node.title, 60)}</div>
                  <div className="date">
                    {new Date(post.node.last_update).toUTCString()}
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default App;
