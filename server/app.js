const express = require("express");
const cors = require("cors");
const { createProxyMiddleware } = require("http-proxy-middleware");

// Creating express object
const app = express();

app.get("/", (req, res) => {
  res.send("Welcome to CORS server 😁");
});

var corsOptions = {
  origin: ["http://localhost:3000"],
};

app.use(cors(corsOptions));

app.use(
  "/api",
  createProxyMiddleware({
    target: "https://englishapi.pinkvilla.com",
    changeOrigin: true,
    pathRewrite: {
      "^/api": "/app-api/v1/photo-gallery-feed-page/page",
    },
  })
);

// Express example
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // Replace with your React app's URL
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.listen(8080, () => {
  console.log("listening on port 8080");
});
